import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import SearchIcon from '@mui/icons-material/Search';
import { Button, Container, Grid, Pagination, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { toast } from 'react-toastify';
import SelectNumberPage from './SelectNuberPage';
import 'react-toastify/dist/ReactToastify.css';
import DeleteModal from './Modal/DeleteModal'
import UpdateModal from './Modal/UpdateModal'
import AddModal from './Modal/AddModal'



function TableProducts() {
    const [posts, setPosts] = useState([]);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);
    const [reLoad, setReLoad] = useState(0);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [openModalAddNewProduct, setOpenModalAddNewProduct] = useState(false);
    const [selectedPost, setSelectedPost] = useState({});
    const [openUpdateModal, setOpenUpdateModal] = useState(false);


    const [show, setShow] = useState(10)

    const limit = show;

    const showAndHideModal = (e) => {
        setOpenUpdateModal(e)
        setReLoad(reLoad + 1)
    }


    const changeHandler = (event, value) => {
        setPage(value);
    }

    const addClick = () => {
     
        console.log("Add");
        setOpenModalAddNewProduct(true);
        setReLoad(reLoad + 1)

    }
    const showAndHideModalAddNewProduct = (e) => {
        setOpenModalAddNewProduct(e)
    }



    const getData2 = async () => {
        const response = await fetch("http://localhost:8080/products");
        const data = await response.json();
        return data;
    }
    const callbackFunction = (childData) => {
        setShow(childData)
        setReLoad(reLoad + 1)
    }
    const getDetailPost = (post) => {
        console.log("Edit");
        setSelectedPost(post);
        setOpenUpdateModal(true);
        setReLoad(reLoad + 1)
    }

    const deleteDetailPost = (post) => {
        console.log("Delete");
        setSelectedPost(post);
        setOpenDeleteModal(true);
        setReLoad(reLoad + 1)
    }
 

    var array = posts
    useEffect(() => {

        getData2()
            .then((data) => {
                console.log(data);
                setNoPage(Math.ceil(data.Products.length / limit));
                setPosts(data.Products.slice((page - 1) * limit, page * limit));

            })
            .catch((error) => {
                console.log(error);
            })

    }, [page, limit, reLoad, openModalAddNewProduct])




    return (
        <>

            <Container maxWidth='xl' style={{ marginLeft: 350 }}>
                <Grid item xs={12} md={12} sm={12} lg={12} mt={1}>
                    <h1>Products</h1>
                </Grid>
                <Grid container marginTop={2}>
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }} >
                        <Button
                            color="success"
                            variant="contained"
                            onClick={() => addClick()}
                            sx={{ m: 1 }}
                            startIcon={<AddIcon />}
                        >Add
                        </Button>
                    </Grid>
                </Grid>
                <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }} >
                    <Grid item xs={2.5} md={2.5} sm={2.5} lg={2.5}>
                        <SelectNumberPage parentCallback={callbackFunction} />
                    </Grid>
                </Grid>
                <Grid item xs={12} md={12} sm={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="posts table">
                            <TableHead sx={{ backgroundColor: 'gray' }}>
                                <TableRow  >
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>STT</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Hình Ảnh</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Tên Sản Phẩm </TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Loại Sản Phẩm </TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Sản Phẩm Khu Vực</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Đơn Giá </TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Giá Khuyến Mãi</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Action</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {posts.map((row, index) => (
                                    <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell component="th" scope="row">{index + 1}</TableCell>
                                        <TableCell><img src={row.ImageUrl} width={100} height={100} alt='hinh'></img></TableCell>
                                        <TableCell>{row.Name}</TableCell>
                                        <TableCell> {row.Type} </TableCell>
                                        <TableCell> {row.Description} </TableCell>
                                        <TableCell>$ {row.BuyPrice} </TableCell>
                                        <TableCell>$ {row.PromotionPrice}</TableCell>
                                        <TableCell>
                                            <Stack spacing={2} direction="row" align='center'>
                                                <Button
                                                    variant="contained"
                                                    onClick={() => getDetailPost(row)}
                                                    startIcon={<EditIcon />}
                                                ></Button>
                                                <Button
                                                    color="error"
                                                    onClick={() => deleteDetailPost(row)}
                                                    startIcon={<DeleteIcon />}
                                                    variant="contained"
                                                >
                                                </Button>
                                            </Stack>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>

                <Grid item xs={12} md={12} sm={12} lg={12} marginTop={5} marginBottom={5}>
                    <Pagination onChange={changeHandler} count={noPage} defaultPage={page}></Pagination>
                </Grid>
                <UpdateModal show={openUpdateModal} onHide={() => setOpenUpdateModal(false)} Products={selectedPost} callBack={showAndHideModal}  />
                <DeleteModal open={openDeleteModal} setOpen={setOpenDeleteModal} post={selectedPost} />
                <AddModal show={openModalAddNewProduct} onHide={() => setOpenModalAddNewProduct(false)} callBack={showAndHideModalAddNewProduct} />
            </Container>
        </>
    )
}

export default TableProducts;
