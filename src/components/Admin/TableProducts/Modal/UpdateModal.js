import { Grid, Table, TableCell, TableHead, TableRow, TextField, TableBody } from '@mui/material';
import { useEffect, useState } from "react";
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';


function ModalEdit(props) {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    const [openModalAddProductInOrder, setOpenModalAddProductInOrder] = useState(false);
    const [showDetailOrder, setShowDetailOrder] = useState([]);
    const [updateDetailOrder, setUpdateDetailOrder] = useState([]);

    const [updateImageUrl, setUpdateImageUrl] = useState(props.Products.ImageUrl);
    const [updateName, setUpdateName] = useState(props.Products.Name);
    const [updateQuantity, setUpdateQuantity] = useState(props.Products.Quantity);
    const [updateType, setUpdateType] = useState(props.Products.Type);
    const [updateDescription, setUpdateDescription] = useState(props.Products.Description);
    const [updateBuyPrice, setUpdateBuyPrice] = useState(props.Products.BuyPrice);
    const [updatePromotionPrice, setUpdatePromotionPrice] = useState(props.Products.PromotionPrice);


    const [reloadPage, setReloadPage] = useState(1);

    const back = () => {
        props.callBack(false)
    }
    const handelUpdateImageUrl = (e) => {
        setUpdateImageUrl(e.target.value)
    }
    const handelUpdateName = (e) => {
        setUpdateName(e.target.value)
    }
    const handelUpdateQuantity = (e) => {
        setUpdateQuantity(e.target.value)
    }
    const handelUpdateType = (e) => {
        setUpdateType(e.target.value)
    }
    const handelUpdateDescription = (e) => {
        setUpdateDescription(e.target.value)
    }
    const handelUpdateBuyPrice = (e) => {
        setUpdateBuyPrice(e.target.value)
    }
    const handelUpdatePromotionPrice = (e) => {
        setUpdatePromotionPrice(e.target.value)
    }
    const xacNhanUpdate = () => {
        var dataUpdate = [showDetailOrder]
        const body = {
            method: 'PUT',
            body: JSON.stringify({

                ImageUrl: updateImageUrl,
                Name: updateName,
                Quantity: updateQuantity,
                Type: updateType,
                Description: updateDescription,
                BuyPrice: updateBuyPrice,
                PromotionPrice: updatePromotionPrice

            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        fetchApi('http://localhost:8080/products/' + props.Products.Id, body)
            .then((data) => {
                console.log(data);
                toast.success("Đã chỉnh sửa Order !");
                setReloadPage(reloadPage + 1)
                props.callBack(false)

            })
            .catch((error) => {
                console.log(error);

            })
    }



    useEffect(() => {
        setShowDetailOrder(props.sap)
        setUpdateDetailOrder(props.sap)
        setUpdateImageUrl(props.Products.ImageUrl)


    }, [props, reloadPage])
    return (
        <>

            <Modal
                size='lg'
                show={props.show}
                onHide={props.onHide}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: 20 }}
            >
                <Modal.Header closeButton>
                    <Modal.Title> ID: {props.Products.Id} - {props.Products.Name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table sx={{ minWidth: 650 }} aria-label="posts table">
                        <TableHead sx={{ backgroundColor: 'gray' }}>
                            <TableRow  >

                                <TableCell sx={{ color: 'white', textAlign: "left", width: "200px" }}>Hình Ảnh</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "400px" }}></TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>

                            <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>

                                <TableCell><img src={updateImageUrl} width={300} height={250} alt='hinh'></img></TableCell>
                                <TableCell>  <TextField id="demo-helper-text-misaligned-no-helper" label="Link Hình Ảnh Sản Phẩm" fullWidth defaultValue={props.Products.ImageUrl} onChange={handelUpdateImageUrl} /> </TableCell>

                            </TableRow>

                        </TableBody>
                    </Table>
                    <hr />
                    <Grid container spacing={2} mt={1}>
                        <Grid item xs={6}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Tên Sản Phẩm" fullWidth defaultValue={props.Products.Name} onChange={handelUpdateName} />
                        </Grid>
                        <Grid item xs={6}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Số Lượng" fullWidth defaultValue={props.Products.Quantity} onChange={handelUpdateQuantity} type='number' />
                        </Grid>

                    </Grid>

                    <Grid container spacing={2} mt={1}>
                        <Grid item xs={6}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Loại Sản Phẩm" fullWidth defaultValue={props.Products.Type} onChange={handelUpdateType} />
                        </Grid>
                        <Grid item xs={6}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Sản Phẩm Khu Vực" fullWidth defaultValue={props.Products.Description} onChange={handelUpdateDescription} />
                        </Grid>

                    </Grid>

                    <Grid container spacing={2} mt={1}>
                        <Grid item xs={6}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Đơn Giá" fullWidth defaultValue={props.Products.BuyPrice} onChange={handelUpdateBuyPrice} type='number' />
                        </Grid>
                        <Grid item xs={6}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Giá Khuyến Mãi" fullWidth defaultValue={props.Products.PromotionPrice} onChange={handelUpdatePromotionPrice} type='number' />
                        </Grid>

                    </Grid>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={back}>
                        Quay Lại
                    </Button>
                    <Button variant="success" onClick={() => xacNhanUpdate()}>Xác Nhận</Button>
                </Modal.Footer>
            </Modal>

        </>
    );
}
export default ModalEdit;
