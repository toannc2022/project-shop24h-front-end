import { Modal, Box, Grid, Button } from "@mui/material";
import { toast } from 'react-toastify';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function DeleteModal({ open, setOpen, post }) {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        await fetch(paramUrl, paramOptions);
    }

    const handleClose = () => setOpen(false);
    const deleteClick = () => {
        const body = {
            method: 'DELETE',
        }

        fetchApi('http://localhost:8080/products/' + post.Id, body)
            .then((data) => {
                console.log(data);
                setOpen(false);
                toast.success("Đã xóa Product !");
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            })

    }
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-detail-title"
            aria-describedby="modal-detail-description"
        >
            <Box sx={style}>
                <h1> Confirm xóa Product</h1>
                <hr />
                <h4>Bạn có chắc chắn muốn xóa Product này không?</h4>

                <Grid textAlign={'right'} mt={5}>

                    <Button variant='contained' sx={{ m: 1 }} color='error' onClick={deleteClick}>Xác nhận</Button>
                    <Button variant='outlined' color='error' onClick={handleClose}>Hủy bỏ</Button>

                </Grid>
            </Box>
        </Modal>
    )
}

export default DeleteModal;
