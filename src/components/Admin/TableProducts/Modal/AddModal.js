import { Grid, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@mui/material';
import { Button, Modal } from 'react-bootstrap';
import { useEffect, useState } from "react";
import { EmailIcon } from '@mui/icons-material/Email';

import { ExpandMoreIcon } from '@mui/icons-material/ExpandMore';
import { toast } from 'react-toastify';



function AddModal(props) {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }

    const [reloadPage, setReloadPage] = useState(1);

    const [updateImageUrl, setUpdateImageUrl] = useState('https://thesocietypages.org/socimages/files/2009/05/nopic_192.gif');
    const [updateName, setUpdateName] = useState('');
    const [updateQuantity, setUpdateQuantity] = useState(0);
    const [updateType, setUpdateType] = useState('');
    const [updateDescription, setUpdateDescription] = useState('');
    const [updateBuyPrice, setUpdateBuyPrice] = useState(0);
    const [updatePromotionPrice, setUpdatePromotionPrice] = useState(0);

    const back = () => {
        props.callBack(false)
    }
    const handelUpdateImageUrl = (e) => {
        setUpdateImageUrl(e.target.value)
    }
    const handelUpdateName = (e) => {
        setUpdateName(e.target.value)
    }
    const handelUpdateQuantity = (e) => {
        setUpdateQuantity(e.target.value)
    }
    const handelUpdateType = (e) => {
        setUpdateType(e.target.value)
    }
    const handelUpdateDescription = (e) => {
        setUpdateDescription(e.target.value)
    }
    const handelUpdateBuyPrice = (e) => {
        setUpdateBuyPrice(e.target.value)
    }
    const handelUpdatePromotionPrice = (e) => {
        setUpdatePromotionPrice(e.target.value)
    }





    const validateOrder = (e) => {
        if (updateImageUrl == 'https://thesocietypages.org/socimages/files/2009/05/nopic_192.gif') {
            toast.error('Vui Lòng Nhập Hình Ảnh!')
            return false;
        }
        if (updateName == '') {
            toast.error('Vui Lòng Nhập Tên Sản Phẩm!')
            return false;
        }
        if (updateQuantity == 0) {
            toast.error('Vui Lòng Nhập Số Lượng Sản Phẩm!')
            return false;
        }
        if (updateType == '') {
            toast.error('Vui Lòng Nhập Loại Sản Phẩm!')
            return false;
        }
        if (updateDescription == '') {
            toast.error('Vui Lòng Nhập Khu Vực Sản Phẩm!')
            return false;
        }
        if (updateBuyPrice == 0) {
            toast.error('Vui Lòng Nhập Đơn Giá!')
            return false;
        }
        if (updatePromotionPrice == 0) {
            toast.error('Vui Lòng Nhập Giá Khuyến Mãi!')
            return false;
        }

        return true;
    }

    const xacNhanAdd = () => {

        if (validateOrder()) {
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    ImageUrl: updateImageUrl,
                    Name: updateName,
                    Quantity: updateQuantity,
                    Type: updateType,
                    Description: updateDescription,
                    BuyPrice: updateBuyPrice,
                    PromotionPrice: updatePromotionPrice
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            }

            fetchApi('http://localhost:8080/products/', body)
                .then((data) => {
                    console.log(data);
                    toast.success("Đã Thêm Product !");
                    setReloadPage(reloadPage + 1)

                    props.callBack(false)

                })
                .catch((error) => {
                    console.log(error);

                })
        }

    }



    useEffect(() => {



    }, [props, reloadPage])
    return (
        <>

            <Modal
                size='lg'
                show={props.show}
                onHide={props.onHide}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: 20 }}
            >
                <Modal.Header closeButton>
                    <Modal.Title> Thêm Sản Phẩm</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table sx={{ minWidth: 650 }} aria-label="posts table">
                        <TableHead sx={{ backgroundColor: 'gray' }}>
                            <TableRow  >

                                <TableCell sx={{ color: 'white', textAlign: "left", width: "200px" }}>Hình Ảnh</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "400px" }}></TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>

                            <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>

                                <TableCell><img src={updateImageUrl} width={300} height={250} alt='hinh'></img></TableCell>
                                <TableCell>  <TextField id="demo-helper-text-misaligned-no-helper" label="Link Hình Ảnh Sản Phẩm" fullWidth onChange={handelUpdateImageUrl} /> </TableCell>

                            </TableRow>

                        </TableBody>
                    </Table>
                    <hr />
                    <Grid container spacing={2} mt={1}>
                        <Grid item xs={6}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Tên Sản Phẩm" fullWidth onChange={handelUpdateName} />
                        </Grid>
                        <Grid item xs={6}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Số Lượng" fullWidth onChange={handelUpdateQuantity} />
                        </Grid>

                    </Grid>

                    <Grid container spacing={2} mt={1}>
                        <Grid item xs={6}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Loại Sản Phẩm" fullWidth onChange={handelUpdateType} />
                        </Grid>
                        <Grid item xs={6}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Sản Phẩm Khu Vực" fullWidth onChange={handelUpdateDescription} />
                        </Grid>

                    </Grid>

                    <Grid container spacing={2} mt={1}>
                        <Grid item xs={6}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Đơn Giá" fullWidth onChange={handelUpdateBuyPrice} />
                        </Grid>
                        <Grid item xs={6}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Giá Khuyến Mãi" fullWidth onChange={handelUpdatePromotionPrice} />
                        </Grid>

                    </Grid>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={back}>
                        Quay Lại
                    </Button>
                    <Button variant="success" onClick={() => xacNhanAdd()}>Xác Nhận</Button>
                </Modal.Footer>
            </Modal>

        </>
    );
}
export default AddModal;
