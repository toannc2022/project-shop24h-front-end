
import Admin from "./Admin";
import TableProducts from "./TableProducts/TableProducts.js";

function AdminProducts() {

    return (
        <>
            <Admin />
            <TableProducts />
        </>
    )
}

export default AdminProducts;
