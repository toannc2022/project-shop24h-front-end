import adminlogo from '../../imageLogo/AdminLogo.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import EditIcon from '@mui/icons-material/Edit';
import SearchIcon from '@mui/icons-material/Search';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import InventoryIcon from '@mui/icons-material/Inventory';
import CalculateIcon from '@mui/icons-material/Calculate';
function Admin() {


    return (
        <>
            <nav className="main-header navbar navbar-expand navbar-white navbar-light">
                <ul className="navbar-nav">

                    <li className="nav-item d-none d-sm-inline-block">
                        <a href="admin" className="nav-link">Home</a>
                    </li>
                    <li className="nav-item d-none d-sm-inline-block">
                        <a href="admin" className="nav-link">Contact</a>
                    </li>
                </ul>
            </nav>
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                <a href="admin" className="brand-link" style={{ textDecoration: 'none' }}>
                    <img src={adminlogo} alt="Logo" className="brand-image img-circle elevation-3" />
                    <span className="brand-text font-weight-light" >Luxury Homes</span>
                </a>
                <div className="sidebar">
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div className="image">
                            <img src='https://orig00.deviantart.net/79f8/f/2012/219/3/8/eternal_mangekyou_sharingan__by_akatsukisasuke1102-d5a5gxh.png' className="img-circle elevation-2" alt="User Image" />
                        </div>
                        <div className="info">
                            <a href="admin" className="d-block" style={{ textDecoration: 'none' }}>Admin</a>
                        </div>
                    </div>
                    <div className="form-inline">
                        <div className="input-group" data-widget="sidebar-search">
                            <input className="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" />
                            <div className="input-group-append">
                                <button className="btn btn-sidebar">
                                    <SearchIcon fontSize='small' />
                                </button>
                            </div>
                        </div>
                    </div>
                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li className="nav-item menu-open">
                                <ul className="nav nav-treeview">

                                    <li className="nav-item" >
                                        <a href="./" className="nav-link" >
                                            <PeopleAltIcon />
                                            <p> Customers</p>
                                        </a>
                                    </li>

                                    <li className="nav-item">
                                        <a href="./products" className="nav-link">
                                            <InventoryIcon />
                                            <p> Products</p>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a href="./orders" className="nav-link">
                                            <CalculateIcon />
                                            <p> Orders</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
        </>
    )


}

export default Admin;