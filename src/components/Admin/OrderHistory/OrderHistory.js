import { Button, Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Stack, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import UpdateModal from "./Modal/UpdateModal";
import SelectNumberPage from "./SelectNumberPage.js";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import InsertModal from "./Modal/InsertModal.js";
import DeleteModal from "./Modal/DeleteModal.js";
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import ModalAddNewOrder from './Modal/InsertModal'

function Datatable() {
    const [posts, setPosts] = useState([]);
    const [dataShow, setDataShow] = useState([]);

    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);
    const [reLoad, setReLoad] = useState(0);

    const [openUpdateModal, setOpenUpdateModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [searhSoDienThoai, setSearhSoDienThoai] = useState();
    const [openModalAddNewOrder, setOpenModalAddNewOrder] = useState(false);
    const [ngayOrderTu, setNgayOrderTu] = useState();
    const [ngayOrderDen, setNgayOrderDen] = useState();

    const showAndHideModalAddNewOrder = (e) => {
        setOpenModalAddNewOrder(e)
    }



    const [show, setShow] = useState(10)

    const limit = show;

    const callbackFunction = (childData) => {
        setShow(childData)
        setReLoad(reLoad + 1)
    }

    const [selectedPost, setSelectedPost] = useState({});
    const [sp, setSp] = useState([]);

    const changeHandler = (event, value) => {
        setPage(value);
    }

    const addClick = () => {
        console.log("Add");
        setOpenModalAddNewOrder(true);
        setReLoad(reLoad + 1)
    }

    const getDetailPost = (row) => {
        console.log("Edit");
        console.log("ID : " + row.Id);
        console.log(row.SanPham[0]);
        setSp(row.SanPham[0])
        setSelectedPost(row);
        setOpenUpdateModal(true);
        setReLoad(reLoad + 1)
    }
    const deleteDetailPost = (post) => {
        console.log("Delete");
        console.log("ID : " + post.id);
        setSelectedPost(post);
        setOpenDeleteModal(true);
        setReLoad(reLoad + 1)
    }
    const getData2 = async () => {
        const response = await fetch("http://localhost:8080/orderbyusers");
        const data = await response.json();
        return data;
    }

    const showAndHideModal = (e) => {
        setOpenUpdateModal(e)
        setReLoad(reLoad + 1)
    }
    const ngayOrderTuHandle = (e) => {
        setNgayOrderTu(e.target.value)
        setReLoad(reLoad + 1)
    }
    const ngayOrderDenHandle = (e) => {
        setNgayOrderDen(e.target.value)
        setReLoad(reLoad + 1)
    }
    var array = dataShow
    useEffect(() => {

        getData2()
            .then((data) => {
                console.log(data.OrderByUsers);
                setNoPage(Math.ceil(data.OrderByUsers.length / limit));
                setPosts(data.OrderByUsers.slice((page - 1) * limit, page * limit));
                setDataShow(data.OrderByUsers);

            })
            .catch((error) => {
                console.log(error);
            })

    }, [page, limit, reLoad, openModalAddNewOrder])

    const searchSDT = (e) => {
        setSearhSoDienThoai(e.target.value)

        if (e.target.value != '') {
            array = posts.filter(
                item => item.SoDienThoai && item.SoDienThoai.includes((e.target.value)
                ))
            setPosts(array)
        } else {
            setReLoad(reLoad + 1)
        }
    }
    const searchDateClick = () => {
        console.log(new Date(ngayOrderTu).getDate());
        console.log(new Date(ngayOrderDen).getDate());

        console.log(array);

        array = dataShow.filter(
            item => new Date(item.TimeCreated).getDate() >= new Date(ngayOrderTu).getDate() && new Date(item.TimeCreated).getDate() <= new Date(ngayOrderDen).getDate())
        setPosts(array)
      

    }

    return (
        <>

            <Container maxWidth='xl' style={{ marginLeft: 350 }}>
                <Grid item xs={12} md={12} sm={12} lg={12} mt={1}>
                    <h1>Orders</h1>
                </Grid>
                <Grid container marginTop={2}>
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }} >
                        <Button
                            color="success"
                            variant="contained"
                            onClick={() => addClick()}
                            sx={{ m: 1 }}
                            startIcon={<AddIcon />}
                        >Add
                        </Button>
                    </Grid>

                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }} >
                        <Grid item xs={2.5} md={2.5} sm={2.5} lg={2.5}>
                            <SelectNumberPage parentCallback={callbackFunction} />
                        </Grid>
                        <Grid item xs={1} md={1} sm={1} lg={1} style={{ marginTop: 7 }}>
                            <TextField
                                label="Số Điện Thoại"
                                variant="standard"
                                fullWidth
                                onChange={searchSDT}
                            />
                        </Grid>
                        <Grid item xs={1} md={1} sm={1} lg={1} style={{ marginTop: 7, marginLeft: 350 }}>
                            <TextField
                                id="date"
                                label="Từ"
                                type="date"

                                sx={{ width: 250 }}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={ngayOrderTuHandle}
                            />

                        </Grid>
                        <Grid item xs={1} md={1} sm={1} lg={1} style={{ marginTop: 7, marginLeft: 150 }}>
                            <TextField
                                id="date"
                                label="Đến"
                                type="date"

                                sx={{ width: 250 }}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={ngayOrderDenHandle}
                            />

                        </Grid>
                        <Grid item xs={1} md={1} sm={1} lg={1} style={{ marginTop: 9, marginLeft: 150 }}>
                            <Button size="large" variant="contained" onClick={searchDateClick}><SearchIcon fontSize="large" /> Search</Button>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={12} sm={12} lg={12}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 650 }} aria-label="posts table">
                                <TableHead sx={{ backgroundColor: 'gray' }}>
                                    <TableRow  >
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Order ID</TableCell>
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Số điện thoại</TableCell>
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Địa Chỉ </TableCell>
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Tổng Hóa Đơn </TableCell>
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Ngày Order </TableCell>
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Trạng Thái</TableCell>
                                        <TableCell sx={{ color: 'white', textAlign: "left" }}>Action</TableCell>

                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {posts.map((row, index) => (
                                        <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                            <TableCell component="th" scope="row">{row.Id}</TableCell>
                                            <TableCell>{row.SoDienThoai}</TableCell>
                                            <TableCell>{row.DiaChi}</TableCell>
                                            <TableCell>$ {row.TongSoTien} </TableCell>
                                            <TableCell>{new Date(row.TimeCreated).toLocaleString()} </TableCell>
                                            <TableCell>Open</TableCell>
                                            <TableCell>
                                                <Stack spacing={2} direction="row" align='center'>
                                                    <Button
                                                        variant="contained"
                                                        onClick={() => getDetailPost(row)}
                                                        startIcon={<EditIcon />}
                                                    ></Button>
                                                    <Button
                                                        color="error"
                                                        onClick={() => deleteDetailPost(row)}
                                                        startIcon={<DeleteIcon />}
                                                        variant="contained"
                                                    >
                                                    </Button>
                                                </Stack>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item xs={12} md={12} sm={12} lg={12} marginTop={5} marginBottom={5}>
                        <Pagination onChange={changeHandler} count={noPage} defaultPage={page}></Pagination>
                    </Grid>
                </Grid>
                <UpdateModal show={openUpdateModal} onHide={() => setOpenUpdateModal(false)} orderByUser={selectedPost} callBack={showAndHideModal} sap={sp} />
                <ModalAddNewOrder show={openModalAddNewOrder} onHide={() => setOpenModalAddNewOrder(false)} callBack={showAndHideModalAddNewOrder} />
                <DeleteModal open={openDeleteModal} setOpen={setOpenDeleteModal} post={selectedPost} />

            </Container>
        </>
    )
}

export default Datatable;
