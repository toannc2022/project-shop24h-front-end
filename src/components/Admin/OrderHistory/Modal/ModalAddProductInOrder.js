import { Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@mui/material';
import { useState } from "react";
import { Button, Modal } from 'react-bootstrap';
import CallAPI from '../../../CallAPI/CallAPI';



function ModalAddProductInOrder(props) {
    const [soLuongAdd, setSoLuongAdd] = useState(1)
    const data = CallAPI();
    const back = () => {
        props.callBackHideModal(false)
    }
    const addSoLuong = (e) => {
        setSoLuongAdd(e.target.value)
    }
    const addProductInOrderClick = (e) => {
        console.log(e)
        props.callBackAddProduct(
            {
                _id: e._id,
                Id: e.Id,
                Name: e.Name,
                Type: e.Type,
                ImageUrl: e.ImageUrl,
                BuyPrice: e.BuyPrice,
                PromotionPrice: e.PromotionPrice,
                Description: e.Description,
                Quantity: soLuongAdd
            }

        )
        setSoLuongAdd(1)
        props.callBackHideModal(false)
    }



    return (
        <Modal
            size='xl'
            show={props.show}
            onHide={props.onHide}
            backdrop="static"
            keyboard={false}
            style={{ marginTop: 0, marginBottom: 50 }}
        >
            <Modal.Header closeButton>
                <Modal.Title>Danh Sách Sản Phẩm</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table sx={{ minWidth: 650 }} aria-label="posts table">
                    <TableHead sx={{ backgroundColor: 'gray' }}>
                        <TableRow  >
                            <TableCell sx={{ color: 'white', textAlign: "left" }}>STT</TableCell>
                            <TableCell sx={{ color: 'white', textAlign: "left" }}>Hình Ảnh</TableCell>
                            <TableCell sx={{ color: 'white', textAlign: "left", width: "200px" }}>Tên Sản Phẩm</TableCell>
                            <TableCell sx={{ color: 'white', textAlign: "left", width: "200px" }}>Đơn Giá</TableCell>
                            <TableCell sx={{ color: 'white', textAlign: "left", width: "120px" }}>Số lượng</TableCell>
                            <TableCell sx={{ color: 'white', textAlign: "center", width: "300px" }}>Action </TableCell>


                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row, index) => (
                            <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                <TableCell >{index + 1}</TableCell>
                                <TableCell><img src={row.ImageUrl} width={100} height={100} alt='hinh'></img></TableCell>
                                <TableCell>{row.Name}</TableCell>
                                <TableCell><p><s>{row.BuyPrice} USD</s><p className="text-danger"> - {row.PromotionPrice} USD </p> </p></TableCell>
                                <TableCell> <TextField
                                    id="outlined-number"
                                    label="Số Lượng"
                                    type="number"
                                    size="small"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    inputProps={{
                                        min: '1'
                                    }}
                                    defaultValue='1'
                                    onChange={addSoLuong}
                                /></TableCell>
                                <TableCell sx={{ textAlign: "center", width: "200px" }}> <Button variant="outline-success" onClick={() => addProductInOrderClick(row)} > Thêm</Button></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <hr />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={back}>
                    Quay Lại
                </Button>

            </Modal.Footer>
        </Modal>
    )
}

export default ModalAddProductInOrder;
