import { Grid, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@mui/material';
import { Button, Modal } from 'react-bootstrap';
import { useEffect, useState } from "react";
import { EmailIcon } from '@mui/icons-material/Email';
import ModalAddProductInOrder from './ModalAddProductInOrder';
import { ExpandMoreIcon } from '@mui/icons-material/ExpandMore';
import { toast } from 'react-toastify';



function ModalAddNewOrder(props) {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    const [openModalAddProductInOrder, setOpenModalAddProductInOrder] = useState(false);
    const [showDetailOrder, setShowDetailOrder] = useState([]);

    const [updateSoDienThoai, setUpdateSoDienThoai] = useState('');
    const [updateDiaChi, setUpdateDiaChi] = useState('');
    const [updateTongSoTien, setUpdateTongSoTien] = useState(0);
    const [soluong, setSoluong] = useState(1);
    const [reloadPage, setReloadPage] = useState(1);

    const showAndHideModalAddProductInOrder = (e) => {
        setOpenModalAddProductInOrder(e)
    }
    const back = () => {
        props.callBack(false)
    }
    const updateDiaChiHandle = (e) => {
        setUpdateDiaChi(e.target.value)
    }
    const updateSoDienThoaiHandle = (e) => {
        setUpdateSoDienThoai(e.target.value)
    }


    const updateSoLuong = (e, id) => {
        setSoluong(e.target.value)
        setReloadPage(reloadPage + 1)

    }
    const updateLaiTongSoTien = (e) => {
        setSoluong(e.target.value)

    }

    const updateXoa = (e, Id) => {

        var db = showDetailOrder.filter(item => item.Id !== Id)
        var tongsotien = 0;
        for (var i = 0; i < db.length; i++) {
            tongsotien = tongsotien + db[i].Quantity * db[i].PromotionPrice
        }
        setUpdateTongSoTien(tongsotien)
        setShowDetailOrder(showDetailOrder.filter(item => item.Id !== Id))
    }

    const updateThem = () => {
        setOpenModalAddProductInOrder(true);
    }

    const addProductCallBack = (e) => {
        setShowDetailOrder([...showDetailOrder, e]);
        setUpdateTongSoTien(parseInt(updateTongSoTien) + e.Quantity * e.PromotionPrice)
    }
    const validateOrder = (e) => {
        if (updateSoDienThoai == '') {
           
            toast.error('Vui Lòng Nhập Số Điện Thoại!')
            return false;
        }
        if (updateDiaChi == '') {
            toast.error('Vui Lòng Nhập Địa Chỉ!')
            return false;
        }
        if (updateTongSoTien == 0) {
            toast.error('Vui Lòng Chọn Sản Phẩm Cần Thanh Toán!')
            return false;
        }

        return true;
    }

    const xacNhanAdd = () => {
        var dataUpdate = [showDetailOrder]
        if (validateOrder()) {
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    SoDienThoai: updateSoDienThoai,
                    DiaChi: updateDiaChi,
                    SanPham: dataUpdate,
                    TongSoTien: updateTongSoTien
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            }

            fetchApi('http://localhost:8080/orderbyusers/', body)
                .then((data) => {
                    console.log(data);
                    toast.success("Đã Thêm Order !");    
                    setReloadPage(reloadPage + 1)
                    setUpdateSoDienThoai('')
                    setUpdateDiaChi('')
                    setUpdateTongSoTien(0)
                    setShowDetailOrder([])

                    props.callBack(false)
                    
                })
                .catch((error) => {
                    console.log(error);

                })
        }

    }



    useEffect(() => {



    }, [props, reloadPage])
    return (
        <>

            <Modal
                size='xl'
                show={props.show}
                onHide={props.onHide}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: 20 }}
            >
                <Modal.Header closeButton>
                    <Modal.Title>ĐƠN HÀNG SĐT </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table sx={{ minWidth: 650 }} aria-label="posts table">
                        <TableHead sx={{ backgroundColor: 'gray' }}>
                            <TableRow  >
                                <TableCell sx={{ color: 'white', textAlign: "left" }}>STT</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left" }}>Hình Ảnh</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "200px" }}>Tên Sản Phẩm</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "200px" }}>Đơn Giá</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "120px" }}>Số lượng</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "center", width: "200px" }}>Thành Tiền </TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "center", width: "200px" }}>Action </TableCell>


                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                showDetailOrder ?
                                    <>
                                        {
                                            showDetailOrder.map((row, index) => (
                                                <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                                    <TableCell >{index + 1}</TableCell>
                                                    <TableCell><img src={row.ImageUrl} width={100} height={100} alt='hinh'></img></TableCell>
                                                    <TableCell>{row.Name}</TableCell>
                                                    <TableCell><p><s>{row.BuyPrice} USD</s><p className="text-danger"> - {row.PromotionPrice} USD </p> </p></TableCell>
                                                    <TableCell> <TextField
                                                        id="outlined-number"
                                                        label="Số Lượng"
                                                        type="number"
                                                        size="small"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        inputProps={{
                                                            min: '1'
                                                        }}
                                                        defaultValue={row.Quantity}
                                                        onChange={(e) => updateSoLuong(e, row.Id)}
                                                        disabled
                                                    /></TableCell>
                                                    <TableCell sx={{ textAlign: "center", width: "200px" }}><h5 id='total-buy'>$ {row.PromotionPrice * row.Quantity}</h5>
                                                    </TableCell>
                                                    <TableCell sx={{ textAlign: "center", width: "200px" }}> <Button variant="outline-danger" onClick={(e) => updateXoa(e, row.Id)}> Xóa</Button></TableCell>

                                                </TableRow>
                                            ))
                                        }
                                    </>
                                    : <h1>Thêm Sản Phẩm</h1>
                            }
                        </TableBody>
                    </Table>
                    <hr />
                    <Grid item xs={12}>
                        <div className="d-grid gap-2">
                            <Button variant="outline-dark" size="lg" onClick={updateThem}> + </Button>
                        </div>
                    </Grid>
                    <hr />
                    <Grid container spacing={2} mt={2}>
                        <Grid item xs={2}>

                        </Grid>
                        <Grid item xs={7}>
                            <h4>Tổng Thanh Toán: </h4>
                        </Grid>
                        <Grid item xs={3} fullWidth >
                            <h4>$ {updateTongSoTien} </h4>
                        </Grid>

                    </Grid>


                    <hr />

                    <Grid container spacing={2} mt={5}>
                        <Grid item xs={4}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="SĐT Người Nhận" fullWidth onChange={updateSoDienThoaiHandle} />
                        </Grid>
                        <Grid item xs={8}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Địa Chỉ Nhận" fullWidth onChange={updateDiaChiHandle} />
                        </Grid>

                    </Grid>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={back}>
                        Quay Lại
                    </Button>
                    <Button variant="success" onClick={() => xacNhanAdd()}>Xác Nhận</Button>
                </Modal.Footer>
            </Modal>
            <ModalAddProductInOrder show={openModalAddProductInOrder} onHide={() => setOpenModalAddProductInOrder(false)} callBackHideModal={showAndHideModalAddProductInOrder} callBackAddProduct={addProductCallBack} />
        </>
    );
}
export default ModalAddNewOrder;
