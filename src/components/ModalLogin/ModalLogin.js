import { Modal, Row, Container, Button } from 'react-bootstrap';
import { TextField, Box } from '@mui/material'
import { auth, googleProvider } from '../../firebase'
import { useState } from 'react';
import { useNavigate, useParams } from "react-router-dom";


function ModalLogin(props) {
    const [user, setUser] = useState(null);
    const [userID, setUserID] = useState(null);
    const [userPW, setUserPW] = useState(null);

    const navigate = useNavigate();

    const loginGoogle = () => {
        auth.signInWithPopup(googleProvider)
            .then((result) => {
                console.log(result);
                setUser(result.user);
                props.callBack(user)
                props.onHide().setModalLoginShow(false)

            })
            .catch((error) => {
                console.log(error)
            })

        navigate("/")
    }

    const signUpUser = () => {
        navigate("/signupuser")
        props.onHide().setModalLoginShow(false)
    }
    const userIDHandle = (e) => {
        setUserID(e.target.value)
    }
    const userPWHandle = (e) => {
        setUserPW(e.target.value)
    }
    const loginClick = () => {
        const getData = async () => {
            const response = await fetch("http://localhost:8080/courses/" + userID);
            const data = await response.json();
            return data;
        }
        getData()
            .then((data) => {
                console.log(data);
                setUser(data)
                props.callBack(user)
            })
            .catch((error) => {
                console.log(error);
            })
    }

    return (
        <div>
            <Modal {...props} aria-labelledby="contained-modal-title-vcenter" style={{ marginTop: 120 }}>
                <Modal.Header closeButton style={{ backgroundColor: '#424242' }}>
                    <Modal.Title  >

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <Container style={{ textAlign: 'center' }}>
                        <h1>Đăng nhập</h1>
                        <Box
                            component="form"
                            sx={{
                                '& .MuiTextField-root': { m: 1, width: '40ch' },
                            }}
                            noValidate
                            autoComplete="off"
                            style={{ textAlign: 'center', margin: 20 }}
                        >

                            <TextField id="outlined-basic" label="Email" variant="outlined" onChange={userIDHandle} />
                            <TextField
                                id="outlined-password-input"
                                label="Mật khẩu"
                                type="password"
                                autoComplete="current-password"
                                onChange={userPWHandle}
                            />

                            <Row style={{ margin: 25, }}>
                                <Button variant="primary" style={{ backgroundColor: '#424242' }} onClick={loginClick}>Đăng nhập</Button>{' '}
                            </Row>
                            <hr />

                            <Button variant="outline-dark" style={{ margin: 20 }}
                                onClick={loginGoogle}
                            >Đăng nhập bằng tài khoản Google  <img
                                    alt=""
                                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png"
                                    width="30"
                                    height="30"
                                    className="logo__img"
                                /></Button>
                            <hr />
                        </Box>
                        <Button variant="outline-primary" style={{ margin: 10 }}
                            onClick={signUpUser}
                        >Tạo tài khoản </Button>
                    </Container>
                </Modal.Body>
                <Modal.Footer style={{ backgroundColor: '#424242' }}>

                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default ModalLogin;
