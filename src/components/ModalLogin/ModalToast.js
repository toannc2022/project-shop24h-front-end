import { Modal, Box, Grid} from "@mui/material";
import '../../App.css';


function ModalToast({ open, setOpen, thongBao }) {

    const handleClose = () => {
        setOpen(false);
        if (thongBao.toastColor === "green") {
            window.location.reload()
        }
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-detail-title"
            aria-describedby="modal-detail-description"
        >
            <Box>
                <Grid>
                    <Grid id="modal__inner">
                        <Grid id="modal__header" style={{ backgroundColor: thongBao.toastColor }}>
                            <h5>Thông báo</h5>
                        </Grid>
                        <Grid id="modal__body" >
                            <h2 style={{ color: thongBao.toastColor }}>{thongBao.toastThongBao}</h2>
                            <p>{thongBao.toastNoiDung}</p>
                        </Grid>
                        <Grid id="modal__footer" >
                            <button onClick={handleClose} style={{ backgroundColor: thongBao.toastColor }}>Close</button>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default ModalToast;
