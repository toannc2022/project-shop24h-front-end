
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import ProductList from './ProductList';

function Products() {
    return (
        <div>
            <Header />
            <ProductList />
            <Footer />
        </div>
    );
}

export default Products;
