import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { Button, Container } from 'react-bootstrap';
import { Card, Grid, TextField, IconButton, Typography, Collapse } from "@mui/material";
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import { useNavigate, useParams } from "react-router-dom";

import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useEffect, useState } from 'react'
import BasicBreadcrumbs from './../Breadcrumbs/BreadcrumbsComponents';
import CallAPI from "../CallAPI/CallAPI";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { styled } from '@mui/material/styles';



const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

function ProductDetail() {
    const [userShoppingCard, setUserShoppingCard] = useState([]);
    const [soluongsanpham, SetSoluongsanpham] = useState(1);

    const [expanded, setExpanded] = useState(true);
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    const { detail } = useParams();
    const [dataMongoDB, setDataMongoDB] = useState([])

    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }




    const navigate = useNavigate();
    const detailProductClick = (element) => {
        navigate("/products/" + element.Description + "/" + element.Id)
        window.scrollTo(0, 0);
    }

    const addToShoppingCard = (element) => {
        console.log(element)
        setUserShoppingCard([...userShoppingCard, element])
        const body = {
            method: 'POST',
            body: JSON.stringify({
                Name: element.Name,
                Type: element.Type,
                ImageUrl: element.ImageUrl,
                BuyPrice: element.BuyPrice,
                PromotionPrice: element.PromotionPrice,
                Quantity: soluongsanpham,
                Description: element.Description
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        fetchApi('http://localhost:8080/shoppingcarts/', body)
            .then((data) => {
                console.log(data);
                navigate("/shopping")

            })
            .catch((error) => {
                console.log(error);
            })


    }
    const addToWishList = (element) => {
        console.log(userShoppingCard)

    }
    const soluongsanphamHandle = (element) => {
        SetSoluongsanpham(element.target.value)

    }
    useEffect(() => {
        const getData = async () => {
            const response = await fetch("http://localhost:8080/products/" + detail);
            const data = await response.json();
            return data;
        }
        getData()
            .then((data) => {
                console.log(data);
                setDataMongoDB(data.Product);

            })
            .catch((error) => {
                console.log(error);
            })
    }, [detail, userShoppingCard])
    const dataDB = CallAPI();
    const array = dataDB.filter(
        item => item.Type && item.Type.includes(dataMongoDB[0].Type),
    )




    return (
        <div>
            <Header />
            <div style={{ marginTop: '150px' }} >


                <Container >
                    {dataMongoDB.map((element, index) => {
                        return (
                            <>
                                <BasicBreadcrumbs props={element} />

                                <Card>
                                    <Grid container>

                                        <Grid sm={7}>
                                            <img src={element.ImageUrl} height='431px' width='690px' alt="img product"></img>
                                        </Grid>
                                        <Grid sm={5} mt={'50px'}>
                                            <h1>{element.Name} </h1>
                                            <p className="text-danger"> - Sale Off {(element.BuyPrice - element.PromotionPrice) / element.BuyPrice * 100 + '%'}</p>
                                            <h4><s>{element.BuyPrice} USD</s><span className="text-danger"> - {element.PromotionPrice} USD </span> </h4>
                                            <Grid container mt={'50px'}>
                                                <Grid sm={2} mt={'10px'}>
                                                    <TextField
                                                        id="outlined-number"
                                                        label="Số Lượng"
                                                        type="number"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        size='small'
                                                        inputProps={{
                                                            min: '1'
                                                        }
                                                        }
                                                        defaultValue='1'
                                                        onChange={soluongsanphamHandle}
                                                    />
                                                </Grid>
                                                <Grid sm={8} mt={'5px'} ml={'10px'}>
                                                    <Button variant="dark" size="lg" style={{ marginBottom: '10px' }} onClick={() => addToShoppingCard(element)}>THÊM VÀO GIỎ HÀNG</Button>
                                                    <Button variant="outline-dark" size="lg" onClick={() => addToWishList(element)} ><FavoriteBorderIcon /> ADD TO WISHLISH </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>

                                    <Collapse in={expanded} timeout="auto" unmountOnExit>
                                        <CardContent>
                                            <Typography paragraph>Thông số sản phẩm:</Typography>
                                            <Typography paragraph>
                                                Mẫu nội thất cao cấp

                                            </Typography>
                                            <Typography paragraph>

                                                Mẫu nội thất đẹp hiện đại nhập khẩu cao cấp phong cách từ nhiều thương hiệu nổi tiếng Châu Á, Châu Âu, Italia (Ý) có nhiều mẫu nội thất phòng khách đẹp đa dạng. Xu hướng chọn mua nội thất phòng khách hiện đại nhập khẩu cao cấp giúp không gian tốt nhất sẽ khiến cho ngôi nhà của bạn trở nên rực rỡ và tiện nghi.
                                            </Typography>


                                        </CardContent>
                                    </Collapse>

                                </Card>
                                <Container style={{ textAlign: 'center' }}>
                                    <ExpandMore
                                        expand={expanded}
                                        onClick={handleExpandClick}
                                        aria-expanded={expanded}
                                        aria-label="show more"
                                    >
                                        <ExpandMoreIcon />
                                    </ExpandMore>
                                </Container>
                            </>
                        )
                    })}
                </Container>

                <Container style={{ marginTop: '100px' }}>
                    <h3>Sản Phẩm Tương Tự</h3>
                    <Grid container mx={15}>
                        {array.map((element, index) => {
                            return (
                                <Grid sm={3} mt={5} mr={3} key={index} >

                                    <Card sx={{ maxWidth: 345 }} className='card__product' onClick={() => detailProductClick(element)} >
                                        <CardMedia
                                            component="img"
                                            height="220"
                                            image={element.ImageUrl}
                                            alt="Paella dish"
                                        />
                                        <CardContent >
                                            <Typography gutterBottom variant="h5" >
                                                {element.Name}
                                            </Typography>
                                            <Typography color="text.secondary" textAlign={'right'}>
                                                <p><s>{element.BuyPrice} USD</s>  -  <span className='text-danger'>   {element.PromotionPrice} USD</span></p>
                                            </Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Grid sm={8}>
                                                <IconButton aria-label="add to favorites">
                                                    <FavoriteIcon />
                                                </IconButton>
                                                <IconButton aria-label="share">
                                                    <ShoppingCartIcon />
                                                </IconButton>
                                            </Grid>
                                            <Grid sm={4}>
                                                <Button variant="outline-success" >Buy Now</Button>
                                            </Grid>

                                        </CardActions>
                                        <Grid >
                                            <Button variant="danger" style={{ marginTop: "-750px", marginLeft: "260px", borderRadius: '50%' }} >-10%</Button>
                                        </Grid>
                                    </Card>

                                </Grid>
                            )
                        })}


                    </Grid>
                </Container>
            </div>


            <Footer />
        </div >
    );
}

export default ProductDetail;
