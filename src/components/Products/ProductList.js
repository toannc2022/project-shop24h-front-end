import { Grid } from '@mui/material'
import ProductFilter from './ProductFilter'
import ProductShow from './ProductShow'
import { useState, useEffect } from 'react';

import CallAPI from '../CallAPI/CallAPI';

function ProductList() {
    const [dataFilter, setDataFilter] = useState(CallAPI())
    const [data, setData] = useState(CallAPI())
  
    const DataSearch = (dataChild) => {
        setDataFilter(dataChild);
        setData(dataChild);
    }
    
    useEffect(() => {
        setData(dataFilter);
      
    }, [dataFilter])
    return (
        <div style={{ marginTop: '100px' }}>

            <Grid container>
                <Grid item sm={1} >
                </Grid>
                <Grid item sm={1.3} >
                    <ProductFilter parentCallback={DataSearch} />
                </Grid>
                <Grid item sm={9} >
                    <ProductShow dataShow={data}/>
                </Grid>
            </Grid>

        </div>
    );
}

export default ProductList;
