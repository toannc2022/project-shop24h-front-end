import { Grid, TextField, FormGroup, FormControlLabel, Checkbox, Box, InputAdornment, Radio, RadioGroup } from '@mui/material'
import { useState, useEffect } from 'react';
import ListIcon from '@mui/icons-material/List';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { Button } from 'react-bootstrap';
import SearchIcon from '@mui/icons-material/Search';

import { useNavigate, useParams } from "react-router-dom";
import CallAPI from '../CallAPI/CallAPI';


function ProductFilter(props) {
    const { id } = useParams();
    const dataDB = CallAPI();


    const [valueRadio, setValueRadio] = useState(id);
    const [filterByName, setFilterByName] = useState('');
    const [filterByMin, setFilterByMin] = useState(0);
    const [filterByMax, setFilterByMax] = useState(9999999);

    const [filterByType1, setFilterByType1] = useState('');
    const [filterByType2, setFilterByType2] = useState('');
    const [filterByType3, setFilterByType3] = useState('');
    const [filterByType4, setFilterByType4] = useState('');
    const [filterByType5, setFilterByType5] = useState('');

    const navigate = useNavigate();

    const nameHandle = (e) => {
        setFilterByName(e.target.value)
    }
    const minHandle = (e) => {
        if (e.target.value != 0) {
            setFilterByMin(e.target.value)
        } else {
            setFilterByMin(0)
        }

    }
    const maxHandle = (e) => {
        if (e.target.value != 0) {
            setFilterByMax(e.target.value)
        } else {
            setFilterByMax(9999999)
        }

    }
    var array = dataDB;

    const tick1 = (e) => {
        if (e.target.checked === true) {
            setFilterByType1(e.target.value)
        } else {
            setFilterByType1('')
        }

    }
    const tick2 = (e) => {
        if (e.target.checked === true) {
            setFilterByType2(e.target.value)
        } else {
            setFilterByType2('')
        }
    }
    const tick3 = (e) => {
        if (e.target.checked === true) {
            setFilterByType3(e.target.value)
        } else {
            setFilterByType3('')
        }
    }
    const tick4 = (e) => {
        if (e.target.checked === true) {
            setFilterByType4(e.target.value)
        } else {
            setFilterByType4('')
        }
    }
    const tick5 = (e) => {
        if (e.target.checked === true) {
            setFilterByType5(e.target.value)
        } else {
            setFilterByType5('')
        }
    }
    const filterCheckbox = (e) => {
        if (filterByType1 === '' && filterByType2 === '' && filterByType3 === '' && filterByType4 === '' && filterByType5 === '') {
            array = dataDB.filter(
                item => item.Name && item.Name.toLowerCase().includes(filterByName.toLowerCase()) && filterByMin <= item.PromotionPrice && item.PromotionPrice <= filterByMax
            )
        } else {
            array = dataDB.filter(
                item => (item.Name && item.Name.toLowerCase().includes(filterByName.toLowerCase()) && filterByMin <= item.PromotionPrice && item.PromotionPrice <= filterByMax && item.Type === filterByType1)
                    || (item.Name && item.Name.toLowerCase().includes(filterByName.toLowerCase()) && filterByMin <= item.PromotionPrice && item.PromotionPrice <= filterByMax && item.Type === filterByType2)
                    || (item.Name && item.Name.toLowerCase().includes(filterByName.toLowerCase()) && filterByMin <= item.PromotionPrice && item.PromotionPrice <= filterByMax && item.Type === filterByType3)
                    || (item.Name && item.Name.toLowerCase().includes(filterByName.toLowerCase()) && filterByMin <= item.PromotionPrice && item.PromotionPrice <= filterByMax && item.Type === filterByType4)
                    || (item.Name && item.Name.toLowerCase().includes(filterByName.toLowerCase()) && filterByMin <= item.PromotionPrice && item.PromotionPrice <= filterByMax && item.Type === filterByType5)
            )

        }
        console.log(array)

        props.parentCallback(array)
    }

    const filterClick = () => {
        navigate("/products/all")
        setValueRadio('all')
        filterCheckbox();
    }
    const allProducts = () => {
        navigate("/products/all")
        filterCheckbox();
        setValueRadio('all')
    }
    const livingroomProducts = () => {
        navigate("/products/livingroom")
        setValueRadio('livingroom')
        array = dataDB.filter(
            item => item.Description && item.Description.includes('livingroom'),
        )
        props.parentCallback(array)
    }
    const bedProducts = () => {
        navigate("/products/bedroom")
        setValueRadio('bedroom')

        array = dataDB.filter(
            item => item.Description && item.Description.includes('bedroom'),
        )
        props.parentCallback(array)
    }
    const kitchenProducts = () => {
        navigate("/products/kitchen")
        setValueRadio('kitchen')

        array = dataDB.filter(
            item => item.Description && item.Description.includes('kitchen'),
        )
        props.parentCallback(array)
    }
    const bathProducts = () => {
        navigate("/products/bathroom")
        setValueRadio('bathroom')

        array = dataDB.filter(
            item => item.Description && item.Description.includes('bathroom'),
        )
        props.parentCallback(array)
    }
    const decorationsProducts = () => {
        navigate("/products/decorations")
        setValueRadio('decorations')

        array = dataDB.filter(
            item => item.Description && item.Description.includes('decorations'),
        )
        props.parentCallback(array)
    }

    useEffect(() => {
        if (id == 'all') {
            props.parentCallback(dataDB)
        } else {
            array = dataDB.filter(
                item => item.Description && item.Description.includes(id),
            )
            props.parentCallback(array)
            console.log(id)
        }

    }, [array])


    return (
        <>
            <div >
                <h5><b><ListIcon /> Tất Cả Danh Mục</b></h5>
                <ul className='fillter__product'>
                    <RadioGroup
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={id}
                        name="radio-buttons-group"
                        value={valueRadio}
                    >
                        <FormControlLabel value="all" control={<Radio />} label="Tất cả sản phẩm" onClick={allProducts} />
                        <FormControlLabel value="livingroom" control={<Radio />} label="Phòng Khách" onClick={livingroomProducts} />
                        <FormControlLabel value="bedroom" control={<Radio />} label="Phòng Ngủ" onClick={bedProducts} />
                        <FormControlLabel value="kitchen" control={<Radio />} label="Nhà Bếp" onClick={kitchenProducts} />
                        <FormControlLabel value="bathroom" control={<Radio />} label="Nhà Tắm" onClick={bathProducts} />
                        <FormControlLabel value="decorations" control={<Radio />} label="Đồ Trang Trí" onClick={decorationsProducts} />
                    </RadioGroup>
                </ul>
            </div>
            <hr />
            <div>
                <h5><b><FilterAltIcon /> Bộ Lọc Tìm Kiếm</b></h5>
            </div>
            <div>

                <Box sx={{ display: 'flex', alignItems: 'flex-end', mt: 4, mb: 3 }}>
                    <TextField
                        id="input-with-icon-textfield"
                        label="Tên Sản Phẩm"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon sx={{ color: 'action.active', mr: 1, my: 1 }} />
                                </InputAdornment>
                            ),
                        }}
                        size='small'
                        onChange={nameHandle}
                    />
                </Box>
            </div>

            <div>
                <p>Theo Danh Mục</p>
                <FormGroup>
                    <FormControlLabel control={<Checkbox onChange={tick1} />} label="Sofa" value='Sofa' />
                    <FormControlLabel control={<Checkbox onChange={tick2} />} label="Bàn" value='table' />
                    <FormControlLabel control={<Checkbox onChange={tick3} />} label="Gương" value='mirror' />
                    <FormControlLabel control={<Checkbox onChange={tick4} />} label="Giường" value='bed' />
                    <FormControlLabel control={<Checkbox onChange={tick5} />} label="Đồ Trang Trí" value='decorations' />
                </FormGroup>
            </div>
            <hr />
            <div>
                <p>Khoảng Giá</p>
                <Grid container>
                    <Grid item sm={5}>
                        <TextField label="Min" size="small" onChange={minHandle} type='number' />
                    </Grid>
                    <Grid item sm={1}>
                    </Grid>
                    <Grid item sm={5}>
                        <TextField label="Max" size="small" onChange={maxHandle} type='number' />
                    </Grid>
                    <Grid item sm={1}>
                    </Grid>
                </Grid>

                <Grid item sm={11} className="d-grid gap-2 mt-4">
                    <Button variant="outline-danger" onClick={filterClick}>Tìm Kiếm</Button>
                </Grid>
            </div>

        </>
    );
}

export default ProductFilter;
