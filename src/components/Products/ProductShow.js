import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';

import FavoriteIcon from '@mui/icons-material/Favorite';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Grid, Typography, IconButton } from '@mui/material'
import { Button } from 'react-bootstrap';

import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import CallAPI from '../CallAPI/CallAPI.js';

export default function ProductShow(props) {
    const [dataFull, setDataFull] = useState(CallAPI());
    const navigate = useNavigate();
    const detailProductClick = (element) => {
        navigate("./" + element.Id)
    }

    useEffect(() => {
        setDataFull(props.dataShow)

    }, [props.dataShow])


    return (
        <>
            <Grid container mx={5}>
                {dataFull.map((element, index) => {
                    return (
                        <Grid item sm={3} mt={5} key={index}>
                            <Card sx={{ maxWidth: 345 }} className='card__product' onClick={() => detailProductClick(element)}>
                                <CardMedia
                                    component="img"
                                    height="220"
                                    image={element.ImageUrl}
                                    alt="Paella dish"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" >
                                        {element.Name}
                                    </Typography>
                                    <Typography color="text.secondary" textAlign={'right'}>
                                        <p><s>{element.BuyPrice} USD</s>  -  <span className='text-danger'>   {element.PromotionPrice} USD</span></p>
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Grid item sm={8}>
                                        <IconButton aria-label="add to favorites">
                                            <FavoriteIcon />
                                        </IconButton>
                                        <IconButton aria-label="share">
                                            <ShoppingCartIcon />
                                        </IconButton>
                                    </Grid>
                                    <Grid item sm={4}>
                                        <Button variant="outline-success" >Buy Now</Button>
                                    </Grid>

                                </CardActions>
                                <Grid >
                                    <Button variant="danger" style={{ marginTop: "-750px", marginLeft: "275px", borderRadius: '50%' }} >-10%</Button>
                                </Grid>
                            </Card>

                        </Grid>
                    )
                })}


            </Grid>
        </>
    );
}