
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import SignUpUser from './SignUpUser'



function SignUp() {


    return (
        <div>
            <Header />
            <SignUpUser />
            <Footer />
        </div>
    );
}

export default SignUp;
