import { useState } from 'react';

import { Card, TextField, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, Container } from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Button } from '@mui/material';
import { useNavigate, useParams } from "react-router-dom";

function SignUpUser() {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    const navigate = useNavigate();
    const [hoTen, setHoTen] = useState('')
    const [soDienThoai, setSoDienThoai] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [values, setValues] = useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
        setPassword(event.target.value)
    };

    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });

    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const nameInputChange = (e) => {
        setHoTen(e.target.value)
    }
    const phoneInputChange = (e) => {
        setSoDienThoai(e.target.value)
    }
    const emailInputChange = (e) => {
        setEmail(e.target.value)
    }
    const signUpCallAPI = (e) => {
        if (validate()) {
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    FullName: hoTen,
                    PhoneNumber: soDienThoai,
                    Email: email,
                    Password: password,
                    Address: password,
                    City: "New 2",
                    Country: "New 2",
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            }
            fetchApi('http://localhost:8080/customers/', body)
                .then((data) => {
                    console.log(data);
                    alert('đăng kí thành công')
                    navigate("/")
                })
                .catch((error) => {
                    console.log(error);

                })

        }
    }
    const validate = () => {
        var mailformat = /^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/;

        if (hoTen === "") {
            console.log("chưa nhập họ tên")
            return false;
        }
        if (soDienThoai === "") {
            console.log("chưa nhập sodienthoai")
            return false;
        }
        if (isNaN(soDienThoai) || soDienThoai.length !== 10) {
            console.log("Số điện thoại phải là số có 10 chữ số!")
            return false;
        }
        if (email !== "" && !email.match(mailformat)) {
            console.log("Email nhập vào không hợp lệ!")
            return false;
        }
        return true;
    }

    return (
        <div>
            <Container style={{ marginTop: '150px', marginBottom: '100px' }} maxWidth='sm'>
                <Card>
                    <div style={{ marginLeft: '35px' }}>

                        <img src="https://www.easyagentpro.com/wp-content/uploads/2015/03/real-estate-logo1.png" className="img-fluid" alt="logo" width={100} height={100} style={{ margin: '20px' }} />
                        <h3 style={{ marginBottom: '25px' }}>
                            Tạo Tài Khoản Luxury Homes
                        </h3>
                        <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                            <TextField
                                id="demo-helper-text-misaligned"
                                label="Họ Tên"
                                size='small'
                                onChange={nameInputChange}
                            />
                        </FormControl>
                        <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                            <TextField

                                id="demo-helper-text-misaligned"
                                label="Số điện thoại"
                                size='small'
                                type='numberphone'
                                onChange={phoneInputChange}
                            />
                        </FormControl>
                        <FormControl sx={{ m: 1, width: '52ch', mt: 2 }} variant="outlined">
                            <TextField
                                helperText="Bạn có thể dùng chữ cái, số và dấu chấm"
                                id="demo-helper-text-misaligned"
                                label="Email"
                                size='small'
                                onChange={emailInputChange}
                            />
                        </FormControl>

                        <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                            <TextField
                                id="outlined-password-input"
                                label="Mật khẩu"
                                type={values.showPassword ? 'text' : 'password'}
                                value={values.password}
                                onChange={handleChange('password')}
                                autoComplete="current-password"
                                size='small'

                            />
                        </FormControl>
                        <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                            <TextField
                                id="outlined-password-input"
                                label="Xác nhận"
                                type={values.showPassword ? 'text' : 'password'}
                                value={values.password}
                                onChange={handleChange('password')}
                                autoComplete="current-password"
                                size='small'
                            />
                        </FormControl>
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                        >
                            {values.showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                        <FormControl sx={{ m: 1, width: '52ch', mt: 3, mb: 5 }} variant="outlined">
                            <Button variant='contained' onClick={signUpCallAPI}> Đăng kí </Button>
                        </FormControl>
                    </div>
                </Card>
            </Container>
        </div>
    );
}

export default SignUpUser;
