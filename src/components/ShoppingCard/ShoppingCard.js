
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import ShoppingCardByUser from './ShoppingCardByUser';

function ShoppingCard() {
    return (
        <div>
            <Header />
            <ShoppingCardByUser />
            <Footer />
        </div>
    );
}

export default ShoppingCard;