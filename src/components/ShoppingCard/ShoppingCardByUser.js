import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { Button, Checkbox, Container, Grid, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import BasicBreadcrumbsSC from "../Breadcrumbs/BreadcrumbsShoppingCart";
import ModalShoppingCart from './ModalShoppingCart';
import { useNavigate } from "react-router-dom";






function ShoppingCardByUser() {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    const [soLuongSP, setSoLuongSP] = useState(1)
    const [totalSP, setTotalSP] = useState(0)
    const [totalKM, setTotalKM] = useState(0)
    const [totalTT, setTotalTT] = useState(0)
    const [modalShoppingCartShow, setModalShoppingCartShow] = useState(false);

    const [checkAll, setCheckAll] = useState(true)


    const [productsShopping, setProductsShopping] = useState([])
    const [orderByUserData, setOrderByUserData] = useState([

    ])


    const deleteProductShopping = (row) => {
        const fetchApi = async (paramUrl, paramOptions = {}) => {
            await fetch(paramUrl, paramOptions);
        }
        const body = {
            method: 'DELETE',
        }

        fetchApi('http://localhost:8080/shoppingcarts/' + row.Id, body)
            .then((data) => {
                console.log(data);
                
            })
            .catch((error) => {
                console.log(error);
            })

    }
    const navigate = useNavigate();
    const getProductShopping = (element) => {
        navigate("/products/" + element.Description)
    }


    const soLuongProduct = (e, Id) => {
        console.log(e.target.value)
        console.log(Id)
        setSoLuongSP(e.target.value);
        const body = {
            method: 'PUT',
            body: JSON.stringify({
                Quantity: e.target.value
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        fetchApi('http://localhost:8080/shoppingcarts/' + Id, body)
            .then((data) => {
                console.log(data);

            })
            .catch((error) => {
                console.log(error);
            })

    }
    const getData = async () => {
        const response = await fetch("http://localhost:8080/shoppingcarts");
        const data = await response.json();
        return data;
    }

    useEffect(() => {

        getData()
            .then((data) => {
                setProductsShopping(data.ShoppingCarts);

            })
            .catch((error) => {
                console.log(error);
            })


    }, [productsShopping, soLuongSP, totalSP])

    const thanhToan = (e, index) => {
        let checked = e.target.checked
        var slsp = 0;
        var km = 0;
        var tt = 0;
        var checkboxes = document.getElementsByName('name[]');
        for (var i = 0; i < productsShopping.length; i++) {
            slsp = slsp + productsShopping[i].Quantity
            km = km + (productsShopping[i].BuyPrice - productsShopping[i].PromotionPrice) * productsShopping[i].Quantity
            tt = tt + productsShopping[i].PromotionPrice * productsShopping[i].Quantity
            setTotalSP(slsp)
            setTotalKM(km)
            setTotalTT(tt)
        }
        if (checkboxes[index].checked = checked) {
            console.log(productsShopping[index])
            setTotalSP(totalSP + productsShopping[index].Quantity)
            setTotalKM(totalKM + (productsShopping[index].BuyPrice - productsShopping[index].PromotionPrice) * productsShopping[index].Quantity)
            setTotalTT(totalTT + productsShopping[index].PromotionPrice * productsShopping[index].Quantity)
            setOrderByUserData([...orderByUserData, productsShopping[index]])
        } else {
            console.log(productsShopping[index])
            setTotalSP(totalSP - productsShopping[index].Quantity)
            setTotalKM(totalKM - (productsShopping[index].BuyPrice - productsShopping[index].PromotionPrice) * productsShopping[index].Quantity)
            setTotalTT(totalTT - productsShopping[index].PromotionPrice * productsShopping[index].Quantity)
            setOrderByUserData(orderByUserData.filter(item => item.Name !== productsShopping[index].Name))

            console.log(index)

            console.log(orderByUserData)

        }


    }


    const checkBoxAllHandle = (e) => {

        var checkboxes = document.getElementsByName('name[]');
        var slsp = 0;
        var km = 0;
        var tt = 0;

        if (checkAll) {
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = true;
                slsp = slsp + productsShopping[i].Quantity
                km = km + (productsShopping[i].BuyPrice - productsShopping[i].PromotionPrice) * productsShopping[i].Quantity
                tt = tt + productsShopping[i].PromotionPrice * productsShopping[i].Quantity

                setTotalSP(slsp)
                setTotalKM(km)
                setTotalTT(tt)
                setOrderByUserData(productsShopping)
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = false;
                setTotalSP(0)
                setTotalKM(0)
                setTotalTT(0)
                setOrderByUserData([])
            }
        }
        setCheckAll(!checkAll)
    }
    const showAndHideModal = (e) => {
        setModalShoppingCartShow(e)
    }

    return (
        <Container>

            <Grid container marginTop={10}>
                <BasicBreadcrumbsSC />
                <Grid item xs={12} md={12} sm={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="posts table">
                            <TableHead sx={{ backgroundColor: 'gray' }}>
                                <TableRow  >
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}><Checkbox color="default" onChange={(e) => checkBoxAllHandle(e)} defaultChecked={!checkAll} /></TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Hình Ảnh</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left", width: "180px" }}>Tên Sản Phẩm</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left", width: "120px" }}>Đơn Giá</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left", width: "110px" }}>Số lượng</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left", width: "150px" }}>Thành Tiền </TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Action</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {productsShopping.map((row, index) => (
                                    <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell ><input type='checkbox' name='name[]' style={{ width: '15px', height: '15px', marginLeft: '15px' }} onChange={(event) => thanhToan(event, index)} value='' /></TableCell>
                                        <TableCell><img src={row.ImageUrl} width={150} height={150}></img></TableCell>
                                        <TableCell>{row.Name}</TableCell>
                                        <TableCell><p><s>{row.BuyPrice} USD</s><p className="text-danger"> - {row.PromotionPrice} USD </p> </p></TableCell>
                                        <TableCell><TextField
                                            id="outlined-number"
                                            label="Số Lượng"
                                            type="number"
                                            size="small"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{
                                                min: '1'
                                            }}
                                            defaultValue={row.Quantity}
                                            onChange={(e) => soLuongProduct(e, row.Id)}
                                        /></TableCell>
                                        <TableCell><h5 id='total-buy'>$ {row.PromotionPrice * row.Quantity}</h5>

                                        </TableCell>

                                        <TableCell>
                                            <Stack spacing={2} direction="row" align='center'>
                                                <Button
                                                    color="error"
                                                    variant="outlined"
                                                    onClick={() => deleteProductShopping(row)}
                                                    startIcon={<DeleteIcon />}
                                                    size="small"
                                                >Xóa </Button>
                                                <Button
                                                    color="inherit"
                                                    variant="text"
                                                    onClick={() => getProductShopping(row)}
                                                    startIcon={<EditIcon />}
                                                    size="small"
                                                >
                                                    Sản Phẩm Tương Tự</Button>
                                            </Stack>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>

            </Grid>

            <Grid container marginTop={10} >

                <Grid item xs={12} md={12} sm={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 350 }} aria-label="posts table">
                            <TableHead >
                                <TableRow  >

                                    <TableCell>TỔNG SẢN PHẨM</TableCell>
                                    <TableCell >TỔNG KHUYẾN MÃI </TableCell>
                                    <TableCell >TỔNG THANH TOÁN</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>

                                <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>

                                    <TableCell>{totalSP}</TableCell>
                                    <TableCell>{totalKM}</TableCell>
                                    <TableCell>{totalTT}</TableCell>

                                </TableRow>

                            </TableBody>

                        </Table>
                        <hr />
                        <Grid item xs={12} md={12} sm={12} lg={12} style={{ textAlign: 'right', marginBottom: '20px' }} container>

                            <Grid item xs={8} md={8} sm={8} lg={8} style={{ marginRight: '50px' }} ><h5>Tổng thanh toán ({totalSP} Sản phẩm): <span className='text-danger'>
                                {totalTT} USD</span></h5>
                            </Grid>
                            <Grid item xs={3} md={3} sm={3} lg={3}>
                                <Button variant="contained" size="large" color='error' fullWidth onClick={() => setModalShoppingCartShow(true)}> THANH TOÁN </Button>
                                <ModalShoppingCart show={modalShoppingCartShow} onHide={() => setModalShoppingCartShow(false)} orderByUser={orderByUserData} totalCost={totalTT} callBack={showAndHideModal} />
                            </Grid>
                        </Grid>
                    </TableContainer>
                </Grid>

            </Grid>

        </Container>
    )
}

export default ShoppingCardByUser;
