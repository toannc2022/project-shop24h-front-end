import { Grid, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@mui/material';
import { useState } from "react";
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';

function ModalShoppingCart(props) {
    const [sodienthoai, setSodienthoai] = useState('')
    const [diachi, setDiachi] = useState('')

    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }

    const back = () => {
        props.callBack(false)
    }
    const sodienthoaiHandle = (e) => {
        setSodienthoai(e.target.value)
    }
    const diachiHandle = (e) => {
        setDiachi(e.target.value)
    }
    const validateOrder = (e) => {
        if (sodienthoai == '') {
            toast.error('Vui Lòng Nhập Số Điện Thoại!')

            return false;
        }
        if (diachi == '') {
            toast.error('Vui Lòng Nhập Địa Chỉ!')

            return false;
        }
        if (props.totalCost == 0) {
            toast.error('Vui Lòng Chọn Sản Phẩm Cần Thanh Toán!')
            return false;
        }


        return true;
    }

    const orderClick = () => {
        // 0 validate sdt va dia chi

        // 1 mo modal yes no va` tao tai khoan

        // no thi tro ve / yes thi put san pham vao
        if (validateOrder()) {

            const body = {
                method: 'POST',
                body: JSON.stringify({
                    SoDienThoai: sodienthoai,
                    DiaChi: diachi,
                    SanPham: [props.orderByUser],
                    TongSoTien: props.totalCost

                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            }

            fetchApi('http://localhost:8080/orderbyusers/', body)
                .then((data) => {
                    console.log(data);
                    toast.success('Order Thành Công!')
                    props.callBack(false)
                    deleteProductShopping(props.orderByUser)
                    setSodienthoai('')
                    setDiachi('')
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }
    const deleteProductShopping = (row) => {
        const fetchApi = async (paramUrl, paramOptions = {}) => {
            await fetch(paramUrl, paramOptions);
        }
        const body = {
            method: 'DELETE',
        }
        for (var i = 0; i < row.length; i++) {
            fetchApi('http://localhost:8080/shoppingcarts/' + row[i].Id, body)
                .then((data) => {
                    console.log(data);
                    window.location.reload();
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    return (
        <>

            <Modal
                size='lg'
                show={props.show}
                onHide={props.onHide}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: 50 }}
            >
                <Modal.Header closeButton>
                    <Modal.Title>ĐƠN HÀNG CỦA BẠN</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table sx={{ minWidth: 650 }} aria-label="posts table">
                        <TableHead sx={{ backgroundColor: 'gray' }}>
                            <TableRow  >
                                <TableCell sx={{ color: 'white', textAlign: "left" }}>STT</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left" }}>Hình Ảnh</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "180px" }}>Tên Sản Phẩm</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "120px" }}>Đơn Giá</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "110px" }}>Số lượng</TableCell>
                                <TableCell sx={{ color: 'white', textAlign: "left", width: "150px" }}>Thành Tiền </TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {props.orderByUser.map((row, index) => (
                                <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                    <TableCell >{index + 1}</TableCell>
                                    <TableCell><img src={row.ImageUrl} width={75} height={75}></img></TableCell>
                                    <TableCell>{row.Name}</TableCell>
                                    <TableCell><p><s>{row.BuyPrice} USD</s><p className="text-danger"> - {row.PromotionPrice} USD </p> </p></TableCell>
                                    <TableCell> {row.Quantity}</TableCell>
                                    <TableCell><h5 id='total-buy'>$ {row.PromotionPrice * row.Quantity}</h5>

                                    </TableCell>


                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    <hr />
                    <Grid container spacing={2} mt={2}>
                        <Grid item xs={2}>

                        </Grid>
                        <Grid item xs={7}>
                            <h4>Tổng Thanh Toán: </h4>
                        </Grid>
                        <Grid item xs={3} fullWidth >
                            <h4>$ {props.totalCost} </h4>
                        </Grid>

                    </Grid>


                    <hr />

                    <Grid container spacing={2} mt={5}>
                        <Grid item xs={4}>
                            <TextField id="demo-helper-text-misaligned-no-helper" label="SĐT Người Nhận" fullWidth onChange={sodienthoaiHandle} />
                        </Grid>
                        <Grid item xs={8}  >
                            <TextField id="demo-helper-text-misaligned-no-helper" label="Địa Chỉ Nhận" fullWidth onChange={diachiHandle} />
                        </Grid>

                    </Grid>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={back}>
                        Quay Lại
                    </Button>
                    <Button variant="success" onClick={orderClick}>Đặt Hàng</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default ModalShoppingCart;
