import * as React from 'react';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material'
import PropTypes from 'prop-types';
import 'video-react/dist/video-react.css';
import Content from './Content';




function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 0 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function TabsContent(props) {
    
    return (
        <div style={{ marginTop: '65px' }}>

            <TabPanel value={props.value} index={0}>
                <Content />
            </TabPanel>
            <TabPanel value={props.value} index={1}>
                Item One
            </TabPanel>
            <TabPanel value={props.value} index={2}>
                Item Three
            </TabPanel>
            <TabPanel value={props.value} index={3}>
                Item Four
            </TabPanel>


        </div>
    );
}

export default TabsContent;
