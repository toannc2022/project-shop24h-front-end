import * as React from 'react';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Grid, Container } from '@mui/material'
import { Card, ListGroup } from 'react-bootstrap'
import MenuIcon from '@mui/icons-material/Menu';
import '../../App.css';
import CarouselWeb from '../Caroucel/Caroucel'
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import PhoneIcon from '@mui/icons-material/Phone';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import StarPurple500Icon from '@mui/icons-material/StarPurple500';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import MoneyIcon from '@mui/icons-material/Money';

const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

function Content() {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <div style={{ textAlign: 'center' }}>
            <Container mt={10} >
                <Grid container mt={12} ml={10} >
                    <Grid item sm={1} mr={-7} mt={1}>
                        <LocalShippingIcon fontSize='large' />
                    </Grid>
                    <Grid item sm={3} >
                        <h5><b> Vận chuyển toàn quốc</b></h5>
                        <p>Qua dịch vụ Viettel post</p>
                    </Grid>
                    <Grid item sm={1} mr={-7} mt={1}>
                        <PhoneIcon fontSize='large' />
                    </Grid>
                    <Grid item sm={3} >
                        <h5><b>Hỗ trợ 24/7</b></h5>
                        <p>Hotline: 0909 804 414</p>
                    </Grid>
                    <Grid item sm={1} mr={-7} mt={1}>
                        <AccessTimeIcon fontSize='large' />
                    </Grid>
                    <Grid item sm={3} >
                        <h5><b> Giờ làm việc</b></h5>
                        <p>8:00 - 21:00 (Từ T2 - CN)</p>
                    </Grid>
                </Grid>
            </Container>
            <Grid container mt={2} style={{ backgroundColor: 'black', padding: 18 }}>
                <Container >
                    <Grid container className='thietke'>
                        <Grid item sm={3} >
                            <a href='/' className='aaa'><h6 className='h5b'><b>TRANG CHỦ</b></h6></a>
                        </Grid>
                        <Grid item sm={3} >
                            <a href='/products/all' className='aaa'><h6 className='h5b'><b>SẢN PHẨM</b></h6></a>
                        </Grid>
                        <Grid item sm={3} >
                            <a href='/' className='aaa'><h6 className='h5b'><b>TIN TỨC</b></h6></a>
                        </Grid>
                        <Grid item sm={3} >
                            <a href='/' className='aaa'><h6 className='h5b'><b>LIÊN HỆ</b></h6></a>
                        </Grid>

                    </Grid>
                </Container>
            </Grid>
            <Container mt={10} >
                <Grid container >
                    <Grid item sm={3} mt={1}>
                        <Card style={{ width: '18rem', height: '500px' }}>
                            <Card.Header style={{ backgroundColor: "grey", color: 'white', padding: 15 }}> <MenuIcon /> Danh Mục</Card.Header>
                            <ListGroup variant="flush" className='thietke'>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/livingroom'>Phòng Khách</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/bathroom'>Nhà Tắm</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/bedroom'>Phòng Ngủ</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/kitchen'>Nhà Bếp</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/decorations'>Đồ Trang Trí</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/livingroom'>Sofa</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/kitchen'>Bàn</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/bathroom'>Gương</a></ListGroup.Item>
                                <ListGroup.Item style={{ padding: 12 }}><a href='products/bedroom'>Giường</a></ListGroup.Item>

                            </ListGroup>
                        </Card>
                    </Grid>
                    <Grid item sm={9} >
                        <CarouselWeb />
                    </Grid>

                </Grid>
            </Container>



            <div style={{ marginTop: '80px' }}>

                <h3 ><b>THIẾT KẾ NGÔI NHÀ CỦA BẠN</b></h3>
                <Container >
                    <Grid container mt={6} className='thietke'>
                        <Grid item sm={3} >
                            <a href='products/livingroom'><img width="277" height="423" alt="PHÒNG KHÁCH" className='img5' src='https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/sofa-670x1024.jpg' />
                                <h6 className='h5b'><b>PHÒNG KHÁCH</b></h6>
                            </a>
                        </Grid>
                        <Grid item sm={3} style={{ marginTop: '50px' }} >
                            <a href='products/bathroom'>
                                <img width="277" height="217" alt="NHÀ TẮM" className='img5' src='https://hdwallpaperim.com/wp-content/uploads/2017/08/25/121299-bathroom-indoors.jpg' />
                                <h6 className='h5b'><b>NHÀ TẮM</b></h6>
                            </a>
                            <a href='products/bedroom' >
                                <img width="277" height="158" alt="PHÒNG NGỦ" className='img5' src='https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/giuong-ngu-1024x586.jpg' style={{ marginTop: '15px' }} />
                                <h6 className='h5b'><b>PHÒNG NGỦ</b></h6>
                            </a>
                        </Grid>
                        <Grid item sm={3} style={{ marginTop: '40px' }} >
                            <a href='products/kitchen'>
                                <img width="277" height="423" alt="NHÀ BẾP" className='img5' src='https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/ban-an-679x1024.jpg' />
                                <h6 className='h5b'><b>NHÀ BẾP</b></h6>
                            </a>
                        </Grid>
                        <Grid item sm={3} >
                            <a href='products/decorations'>
                                <img width="277" height="355" alt="ĐỒ TRANG TRÍ" className='img5' src='https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/trang-tri-1024x1312.jpg' />
                                <h6 className='h5b'><b>ĐỒ TRANG TRÍ</b></h6>
                            </a>
                        </Grid>
                    </Grid>
                </Container>
            </div>
            <div style={{ marginTop: '80px', backgroundColor: '#eeeeee', padding: 1 }}>
                <h3 style={{ marginTop: '50px' }}><b>TẠI SAO CHỌN CHÚNG TÔI?</b></h3>
                <Grid container mt={2} style={{ backgroundColor: '#eeeeee' }}>

                    <Container >
                        <Grid container className='thietke' mt={3}>
                            <Grid item sm={1} mt={2} mr={-3}>
                                <StarPurple500Icon fontSize='large' />
                            </Grid>

                            <Grid item sm={3} >
                                <h5><b>Sản phẩm chính hãng</b></h5>
                                <p>Chúng tôi cam kết 100% sản phẩm là hàng chính hãng, chất lượng cao.</p>
                            </Grid>
                            <Grid item sm={1} mt={2} mr={-3}>
                                <VerifiedUserIcon fontSize='large' />
                            </Grid>
                            <Grid item sm={3} >
                                <h5><b>Bảo hành chuyên nghiệp</b></h5>
                                <p>Dịch vụ bảo hành tận nơi chuyên nghiệp, tận tình và chu đáo cho khách hàng.</p>
                            </Grid>
                            <Grid item sm={1} mt={2} mr={-3}>
                                <MoneyIcon fontSize='large' />
                            </Grid>
                            <Grid item sm={3} >
                                <h5><b>Giá tốt nhất trên thị trường</b></h5>
                                <p>Tự tin là nhà cung cấp sản phẩm nội thất với giá cả tốt nhất Việt Nam.</p>
                            </Grid>

                        </Grid>
                    </Container>
                </Grid>
            </div>

            <Container >
                <ExpandMore
                    expand={expanded}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </ExpandMore>
            </Container>


        </div >
    );
}

export default Content;
