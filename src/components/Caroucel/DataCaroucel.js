const DataCaroucel = {
    description: "Images in Home",
    data: [
        {
            id: 1,
            type: "PhongKhach",
            imgUrl: "https://images3.alphacoders.com/100/thumb-1920-1003403.jpg"
        },
        {
            id: 2,
            type: "PhongNgu",
            imgUrl: "https://www.bhmpics.com/wallpapers/bedroom_interior_design_blue-1920x1080.jpg"
        },
        {
            id: 3,
            type: "NhaBep",
            imgUrl: "https://cdn.wallpapersafari.com/51/41/xKEkYd.jpg"
        },
        {
            id: 4,
            type: "NhaTam",
            imgUrl: "https://woodhouse-sturnham.co.uk/wp-content/uploads/2016/11/blurb2.jpg"
        },
    ]
}

export default DataCaroucel;
