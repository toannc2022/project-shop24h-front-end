import * as React from 'react';
import { Carousel, Container } from 'react-bootstrap';
import DataCaroucel from './DataCaroucel.js'

export default function CarouselWeb() {
    return (
        <Container fluid className='p-0 mt-2' >
            <Carousel className="d-block">
                {DataCaroucel.data.map((element, index) => {
                    return (
                        <Carousel.Item key={index}>
                            <img
                                className="d-block w-100"
                                src={element.imgUrl}
                                alt="First slide"
                                style={{ height: '500px' }}
                            />
                            <Carousel.Caption>
                              
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })}
            </Carousel>
        </Container>

    );
}
