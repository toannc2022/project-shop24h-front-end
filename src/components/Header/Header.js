import * as React from 'react';
import { Navbar, Nav, Container, NavDropdown, Button } from 'react-bootstrap';
import { Grid } from '@mui/material/'
import ChatBubbleIcon from '@mui/icons-material/ChatBubble';
import CircleNotificationsIcon from '@mui/icons-material/CircleNotifications';
import SettingsIcon from '@mui/icons-material/Settings';
import { useState, useEffect } from 'react';
import ModalLogin from '../ModalLogin/ModalLogin';
import { auth } from '../../firebase';
import TabsIcon from './TabsIcon';
import EditIcon from '@mui/icons-material/Edit';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useNavigate, useParams } from "react-router-dom";
function Header(props) {

    const [modalLoginShow, setModalLoginShow] = useState(false);
    const [user, setUser] = useState(null);
    const [dataShoppingCart, setDataShoppingCart] = useState([]);

    const navigate = useNavigate();

    const logout = () => {
        auth.signOut()
            .then(() => {
                setUser(null)
            })
            .catch((error) => {
                console.log(error)
            })
    }
    const callBackUser = (user) => {
        setUser(user)
    }
    useEffect(() => {
        auth.onAuthStateChanged((result) => {
            setUser(result);
        })
    }, [])



    const modifyMessage = (data) => {

        props.parentCallback(data)
    }
    const shoppingCartClick = () => {
        navigate("/shopping")
        const getData = async () => {
            const response = await fetch("http://localhost:8080/courses/622c63d82980cef75d60d2e9/reviews");
            const data = await response.json();
            return data;
        }
        getData()
            .then((data) => {
                console.log(data);
                setDataShoppingCart(data)
            })
            .catch((error) => {
                console.log(error);
            })
    }


    return (
        <div>
            <Navbar bg="dark" variant="dark" fixed='top'>
                <Container>
                    <Grid item xs={4}>
                        <Navbar.Brand href="/">
                            <img
                                alt=""
                                src="https://www.easyagentpro.com/wp-content/uploads/2015/03/real-estate-logo1.png"
                                width="50"
                                height="30"
                                className="d-inline-block align-top"
                            />{' '}
                            Luxury Homes
                        </Navbar.Brand>
                    </Grid>
                    <Grid item xs={6}>
                        <TabsIcon parentCallback={modifyMessage} />
                    </Grid>
                    <Grid item xs={3}>
                        <Nav className="justify-content-end" activeKey="/">

                            <Nav.Item>
                                <Nav.Link eventKey="link-1"><ChatBubbleIcon /></Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="/shopping" onClick={shoppingCartClick}><ShoppingCartIcon /></Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="link-3"><CircleNotificationsIcon /></Nav.Link>
                            </Nav.Item>

                            {
                                user ?
                                    <>
                                        <NavDropdown title={<img
                                            alt=""
                                            src="https://orig00.deviantart.net/79f8/f/2012/219/3/8/eternal_mangekyou_sharingan__by_akatsukisasuke1102-d5a5gxh.png"
                                            width="30"
                                            height="30"
                                            className="logo__img"
                                        />}
                                            menuVariant="dark">
                                            <NavDropdown.Item eventKey="4.1" style={{ textAlign: 'center' }}><img
                                                alt=""
                                                src="https://orig00.deviantart.net/79f8/f/2012/219/3/8/eternal_mangekyou_sharingan__by_akatsukisasuke1102-d5a5gxh.png"
                                                width="30"
                                                height="30"
                                                className="logo__img"
                                            /></NavDropdown.Item>
                                            <NavDropdown.Item eventKey="4.2" style={{ textAlign: 'center' }}>{user.displayName}</NavDropdown.Item>
                                            <NavDropdown.Divider />
                                            <NavDropdown.Item eventKey="4.3"><EditIcon />  Edit Profile</NavDropdown.Item>
                                            <NavDropdown.Divider />
                                            <NavDropdown.Item eventKey="4.4"><SettingsIcon />  Setting</NavDropdown.Item>
                                            <NavDropdown.Divider />
                                            <NavDropdown.Item eventKey="4.5" style={{ textAlign: 'center' }}><div className="d-grid gap-2"><Button variant="outline-light" onClick={logout}>Log Out</Button></div></NavDropdown.Item>
                                        </NavDropdown>

                                    </>
                                    :
                                    <Button variant="outline-light" onClick={() => setModalLoginShow(true)}>
                                        Sign Up
                                    </Button>
                            }

                        </Nav>
                    </Grid>

                </Container>

            </Navbar>




            <ModalLogin show={modalLoginShow} onHide={() => setModalLoginShow(false)} callBack={callBackUser} />
        </div>
    );
}

export default Header;
