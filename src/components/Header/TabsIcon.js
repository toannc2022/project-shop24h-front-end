import * as React from 'react';
import PropTypes from 'prop-types';

import Typography from '@mui/material/Typography';
import { useNavigate, useParams } from "react-router-dom";
import { ThemeProvider } from '@mui/material/styles';
import { Tabs, Tab, createTheme, Box } from '@mui/material/'
import HomeIcon from '@mui/icons-material/Home';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import VideoLibraryIcon from '@mui/icons-material/VideoLibrary';
const theme = createTheme({
    palette: {
        secondary: {
            light: '#fafafa',
            main: '#fafafa',
            dark: '#fafafa',
            contrastText: '#fafafa',
        },
    },
});
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function TabsIcon(props) {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
        props.parentCallback(newValue)
    };
    const navigate = useNavigate();
    const OrderHistory = () => {
        navigate("/orderhistory")
    }
    const productList = () => {
        navigate("/products/all")
    }

    return (
        <>
            <ThemeProvider theme={theme}>
                <Box sx={{ width: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>

                        <Tabs aria-label="icon tabs example" value={value}
                            onChange={handleChange} indicatorColor='secondary' >
                            <Tab icon={<HomeIcon style={{ color: "#f5f5f5" }} />} aria-label="home" {...a11yProps(0)} />
                            <Tab icon={<FavoriteIcon style={{ color: "#f5f5f5" }} />} aria-label="favorite"  {...a11yProps(1)} />
                            <Tab icon={<PeopleAltIcon style={{ color: "#f5f5f5" }} />} aria-label="group"  {...a11yProps(2)} onClick={productList}/>
                            <Tab icon={<VideoLibraryIcon style={{ color: "#f5f5f5" }} />} aria-label="video" onClick={OrderHistory} />
                        </Tabs>
                    </Box>
                </Box>
            </ThemeProvider>

        </>
    );
}