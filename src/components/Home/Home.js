import '../../App.css';
import { useState } from 'react';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import TabsContent from '../Content/TabsContent';



function Home() {

    const [childData, setChildData] = useState(0)
    const modifyMessage = (data) => {
        setChildData(data)
    }
    return (
        <div>
            <Header parentCallback={modifyMessage} />
            <TabsContent value={childData} />
            <Footer />

        </div>
    );
}

export default Home;
