import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaperPlane, faPhone, faMagnifyingGlass, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faYoutube, faInstagram, faTiktok } from '@fortawesome/free-brands-svg-icons'
import EmailIcon from '@mui/icons-material/Email';



function Footer() {
    return (
        <div className="mt-5 mb-0">
            <footer className="footer-section">
                <div className="container">
                    <div className="footer-cta pt-5 pb-5">
                        <div className="row">
                            <div className="col-xl-4 col-md-4 mb-30">
                                <div className="single-cta">
                                    <FontAwesomeIcon icon={faMagnifyingGlass} style={{ color: 'whitesmoke' }} />
                                    <div className="cta-text">
                                        <h4>Find us</h4>
                                        <span>Giờ Mở Cửa: 8:30 - 21:00 (Mỗi Ngày)</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-md-4 mb-30">
                                <div className="single-cta">
                                    <FontAwesomeIcon icon={faPhone} style={{ color: 'whitesmoke' }} />

                                    <div className="cta-text">
                                        <h4>Call us</h4>
                                        <span>+84 909 804 414</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-md-4 mb-30">
                                <div className="single-cta">
                                    <EmailIcon style={{ color: 'whitesmoke' }} />
                                    <div className="cta-text">
                                        <h4>Mail us</h4>
                                        <span>canhtoan95@gmail.com</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer-content pt-5 pb-5">
                        <div className="row">
                            <div className="col-xl-4 col-lg-4 mb-50">
                                <div className="footer-widget">
                                    <div className="footer-logo">
                                        <a href="index.html"><img src="https://www.easyagentpro.com/wp-content/uploads/2015/03/real-estate-logo1.png" className="img-fluid" alt="logo" /></a>
                                    </div>
                                    <div className="footer-text">
                                        <FontAwesomeIcon icon={faMapMarkerAlt} style={{ color: 'whitesmoke' }} />  <div className="cta-text">
                                            <span>TP. Hồ Chí Minh - Việt Nam</span>
                                        </div>
                                    </div>
                                    <div className="footer-social-icon" style={{ marginTop: '20px' }}>
                                        <span>Follow us</span>
                                        <a href="/"><FontAwesomeIcon icon={faFacebook} className='google-bg' /></a>
                                        <a href="/"><FontAwesomeIcon icon={faYoutube} className='google-bg' /></a>
                                        <a href="/"><FontAwesomeIcon icon={faInstagram} className='google-bg' /></a>
                                        <a href="/"><FontAwesomeIcon icon={faTiktok} className='google-bg' /></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-6 mb-30">
                                <div className="footer-widget">
                                    <div className="footer-widget-heading">
                                        <h3>Useful Links</h3>
                                    </div>
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="/">about</a></li>
                                        <li><a href="/">services</a></li>
                                        <li><a href="/">portfolio</a></li>
                                        <li><a href="/">Contact</a></li>
                                        <li><a href="/">About us</a></li>
                                        <li><a href="/">Our Services</a></li>
                                        <li><a href="/">Expert Team</a></li>
                                        <li><a href="/">Contact us</a></li>
                                        <li><a href="/">Latest News</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-6 mb-50">
                                <div className="footer-widget">
                                    <div className="footer-widget-heading">
                                        <h3>Subscribe</h3>
                                    </div>
                                    <div className="footer-text mb-25">
                                        <p>Don’t miss to subscribe to our new feeds, kindly fill the form below.</p>
                                    </div>
                                    <div className="subscribe-form">
                                        <form action="#">
                                            <input type="text" placeholder="Email Address" />
                                            <button><FontAwesomeIcon icon={faPaperPlane} /></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="copyright-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-6 col-lg-6 text-center text-lg-left">
                                <div className="copyright-text">
                                    <p>Copyright &copy; 2020, All Right Reserved <a href="/">Akatsuki</a></p>
                                </div>
                            </div>
                            <div className="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                                <div className="footer-menu">
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="/">Terms</a></li>
                                        <li><a href="/">Privacy</a></li>
                                        <li><a href="/">Policy</a></li>
                                        <li><a href="/">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default Footer;
