import { useState, useEffect } from 'react'



function CallAPI() {
    const [dataMongoDB, setDataMongoDB] = useState([])
    const getData = async () => {
        const response = await fetch("http://localhost:8080/products/");

        const data = await response.json();

        return data;
    }
    useEffect(() => {
        getData()
            .then((data) => {
       
                setDataMongoDB(data.Products);
            })
            .catch((error) => {
                console.log(error);
            })
    }, [])

    return (
        dataMongoDB
    )


}

export default CallAPI;