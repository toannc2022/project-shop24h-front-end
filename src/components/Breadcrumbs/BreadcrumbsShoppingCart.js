import * as React from 'react';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';



export default function BasicBreadcrumbsSC() {
    return (
        <div role="presentation" style={{ margin: '25px' }}>
            <Breadcrumbs aria-label="breadcrumb">
                <Link underline="hover" color="inherit" href="/">
                    Home
                </Link>
                <Link
                    underline="hover"
                    color="inherit"
                    href="/Products/all"
                >
                    Products
                </Link>

                <Typography color="text.primary">Shopping Cart</Typography>
            </Breadcrumbs>
        </div>
    );
}