import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route } from "react-router-dom";

import Products from './components/Products/Products'
import Home from './components/Home/Home.js'

import ProductDetail from './components/Products/ProductDetail';
import ShoppingCard from './components/ShoppingCard/ShoppingCard';
import SignUp from './components/SignUp/SignUP';
import OrderHistory from './components/Admin/OrderHistory/OrderHistory.js'
import AdminOrders from './components/Admin/AdminOrders';
import AdminProducts from './components/Admin/AdminProducts';

import { ToastContainer, toast } from 'react-toastify';
function App() {


  return (
    <div>

      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route exact path="/products" element={<Products />}></Route>
        <Route exact path="/products/:id" element={<Products />}></Route>
        <Route exact path="/products/:id/:detail" element={<ProductDetail />}></Route>
        <Route exact path="/shopping" element={<ShoppingCard />}></Route>
        <Route exact path="/signupuser" element={<SignUp />}></Route>
        <Route exact path="/orderhistory" element={<OrderHistory />}></Route>
        <Route exact path="/admin/orders" element={<AdminOrders />}></Route>
        <Route exact path="/admin/products" element={<AdminProducts />}></Route>


        <Route path="*" element={<Home />}></Route>
      </Routes>
      <ToastContainer />


    </div>
  );
}

export default App;
