import firebase from 'firebase/app';

import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyC445z8bae76qtQZK-WWdECLFGuw8ijUZw",
    authDomain: "devcamp-shop-24h-34427.firebaseapp.com",
    databaseURL: "https://devcamp-shop-24h-34427-default-rtdb.firebaseio.com",
    projectId: "devcamp-shop-24h-34427",
    storageBucket: "devcamp-shop-24h-34427.appspot.com",
    messagingSenderId: "1055284576842",
    appId: "1:1055284576842:web:7c274a5ad6a207f4c21cb3"
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();
