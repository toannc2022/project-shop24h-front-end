const dataDB = [
    {
        Id: '6223a6cc95267241bd0a3611',
        Name: 'Black - Sofa',
        Type: 'Sofa',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/18f9bc26-lazio-hm2goc.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'livingroom',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 12,
        Name: 'White - Sofa',
        Type: 'Sofa',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/572fc207-lazio_214goc.2.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'livingroom',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 21,
        Name: 'Brow - Bed',
        Type: 'bed',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/04/OPERA.RAT-PHAN.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'bedroom',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 22,
        Name: 'Red - Bed',
        Type: 'bed',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/04/PILLOWY.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'bedroom',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 31,
        Name: 'Kitchen - Table 01',
        Type: 'table',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/04/SYDNEY.1-800x500.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'kitchen',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 32,
        Name: 'Kitchen - Table 02',
        Type: 'table',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/03/VERONA.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'kitchen',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 41,
        Name: 'Kệ Trang Trí - 01',
        Type: 'decorations',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/06/HKM03-MD03-MFC-204SH.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'decorations',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 42,
        Name: 'Kệ Trang Trí - 02',
        Type: 'decorations',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2021/04/GRACIE.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'decorations',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 51,
        Name: 'Gương - Tròn',
        Type: 'mirror',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2022/01/Guong-HH0008146.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'bathroom',
        TimeCreated: '',
        TimeUpdated: ''
    },
    {
        Id: 52,
        Name: 'Gương - Vuông',
        Type: 'mirror',
        ImageUrl: 'https://d3dmq92854myzj.cloudfront.net/wp-content/uploads/2022/01/Guong-HH0008149.1.jpg',
        BuyPrice: 1000,
        PromotionPrice: 900,
        Description: 'bathroom',
        TimeCreated: '',
        TimeUpdated: ''
    },

]


export default dataDB;